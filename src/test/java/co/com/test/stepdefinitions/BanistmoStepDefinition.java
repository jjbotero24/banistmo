package co.com.test.stepdefinitions;

import co.com.test.tasks.BanistmoTask;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import question.Answer;


public class DemoStepDefinition {

    @Before
    public void setTheStage(){
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("^que actor quiere consultar el PDF$")
    public void queActorQuiereConsultarElPDF() {
        OnStage.theActor("Actor").wasAbleTo(BanistmoTask.url());
    }

    @Then("^el deberia ver el resultado$")
    public void elDeberiaVerElResultado() {
        String question = "Categorías";
        OnStage.theActorInTheSpotlight().should(GivenWhenThen.seeThat(Answer.toThe(question)));
    }
}
