package co.com.test.tasks;

import co.com.test.userinterface.ChoucairLoginPage;
import co.com.test.userinterface.BanistmoPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Open;

public class OpenThe implements Task {

    private BanistmoPage demopage;

    public static OpenThe url() {
        return Tasks.instrumented(OpenThe.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Open.browserOn(demopage));

        actor.attemptsTo(Click.on(ChoucairLoginPage.BTN_LOGIN ));
        actor.attemptsTo(Enter.theValue("jjbotero").into(ChoucairLoginPage.TXT_USER));
        actor.attemptsTo(Enter.theValue("Jero26bibia").into(ChoucairLoginPage.TXT_PASSWORD));
        actor.attemptsTo(Click.on(ChoucairLoginPage.BTN_ACEPTAR ));

    }
}
