package co.com.test.userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class ControloesPage extends PageObject {

    public static final Target APRENDER_ES_FACIL  = Target.the("Aprender")
            .located(By.xpath("//a[contains(text(),'Aprender es Fácil')]"));
    public static final Target LEGALES = Target.the("Legales")
            .located(By.xpath("//h1[contains(text(),'Aprender es fácil')]//following::h3[contains(text(),'Legales')]//following::a[contains(text(),'Descubre más')]"));
    public static final Target FACTACS = Target.the("Tas")
            .located(By.xpath("//h3[contains(text(),'FATCA & CRS')]//following::a[contains(text(),'Conoce más')]"));
    public static final Target PDF = Target.the("PDF")
            .located(By.xpath("//h1[contains(text(),'FATCA & CRS')]//following::strong[contains(text(),'PJ-Autocertificación unificado- Fatca y CRS')]//following::img[@alt='Descargar PDF']"));

    public static final Target NOMBRE_PDF = Target.the("NombrePdf")
            .located(By.xpath("//title[contains(text(),'PJ+Autocertificacion+unificado-+Fatca+y+CRS.pdf')]"));
}
