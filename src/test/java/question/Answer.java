package question;

import co.com.test.userinterface.ControloesPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class Answer implements Question<Boolean> {

    private String question;

    public Answer( String question){
        this.question = question;
    }

    public static Answer toThe( String question){
        return new Answer(question);
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        boolean result;

        String  nombrePdf = Text.of(ControloesPage.NOMBRE_PDF).viewedBy(actor).asString();

        result = question.equals(nombrePdf);

        return result;
        }
}

